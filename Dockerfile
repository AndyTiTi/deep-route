FROM node:12-alpine3.14 as builder
WORKDIR /app
COPY  package.json /app
RUN npm install -g cnpm --registry=https://registry.npm.taobao.org
RUN cnpm install
COPY . /app
RUN npm run build

FROM nginx:latest
COPY --from=builder /app/dist /usr/share/nginx/html